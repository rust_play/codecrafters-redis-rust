use bytes::BytesMut;
use tokio::{io::{AsyncReadExt, AsyncWriteExt as _}, net::TcpStream};
use anyhow::Result;

pub struct Client {
    socket: TcpStream,
    buf: BytesMut,
}

impl Client {
    pub async fn new(addr: std::net::SocketAddr) -> Result<Self> {
        let socket = TcpStream::connect(addr).await?;
        Ok(Self {
             socket,  
             buf: BytesMut::with_capacity(1024)
        })
    }
    pub async fn send_request(&mut self, src: &[u8]) -> Result<Vec<u8>> {
        println!("Client-Redis: sending request");
        self.socket.write_all(src).await?;
        self.socket.flush().await?;
        self.socket.shutdown().await?;

        println!("Client-Redis: response from server");
        let bytes_read = self.socket.read_buf(&mut self.buf).await?;
        println!("Client-Redis: Read {} bytes: {:?}", bytes_read, self.buf);
        Ok(self.buf.to_vec())
    }
}
