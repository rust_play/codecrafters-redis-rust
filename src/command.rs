use std::ops::Deref;
use std::sync::Arc;

use tokio::time::Instant;
use crate::resp::FormattedInput;
use crate::db::{DBHandle, DBStorage};

pub struct Command ();
impl Command {

    pub async fn execute_command(str: &str, args: Vec<FormattedInput<String>>, db: Arc<DBHandle>) -> FormattedInput<String> {
        match str {
             "set" => Self::process_set_command(args, db).await,
             "get" => Self::process_get_command(args, db).await,
             "ping" => FormattedInput::SimpleString("PONG".to_string()),
             "echo" => args.first().unwrap().clone(),
             c => panic!("Cannot handle command {}", c),
        }
    }

    pub async fn process_set_command(args: Vec<FormattedInput<String>>, db: Arc<DBHandle>) -> FormattedInput<String> {
        let mut time_out_value = None;
        let key_string = Self::unpack_formatted_str(&args.first().unwrap().clone()).unwrap();
        let value_string =  Self::unpack_formatted_str(&args[1].clone()).unwrap();

        if &args.len()> &2 {
            time_out_value = Some(Self::unpack_formatted_str(&args[3].clone()).unwrap().parse::<u64>().unwrap());
        }

        let mut db  = db.deref().to_owned();
        db.set(&key_string,  DBStorage::new(value_string, time_out_value)).await;
        return FormattedInput::SimpleString("OK".to_string());
    }

    pub async fn process_get_command(args: Vec<FormattedInput<String>>,  db: Arc<DBHandle>) -> FormattedInput<String> {
        let mut db  = db.deref().to_owned();
        let key_string = Self::unpack_formatted_str(&args.first().unwrap().clone()).unwrap();
        let db_command = db.get(key_string.to_string()).await;
        if let Some(line) = db_command
        {
            return FormattedInput::BulkString(line.to_string());
        } else {
            return FormattedInput::NoneString("-1".to_string());
        };
    }

    pub fn extract_command(value: FormattedInput<String>) -> Result<(String, Vec<FormattedInput<String>>), anyhow::Error> {
        let mut v: Vec<FormattedInput<String> > = Vec::new();
        v.push(value.clone());
        match value {
            FormattedInput::Array(a) => {
                Ok((
                    Self::unpack_formatted_str(&a.first().unwrap().clone())?,
                    a.into_iter().skip(1).collect(),
                ))
            },
            FormattedInput::BulkString(_s) => Ok(( "echo".to_string(), v)),
            FormattedInput::SimpleString(_s) => Ok(( "echo".to_string(), v)),
            _ => Err(anyhow::anyhow!("Unexpected command format"))
        }
    }
    
    pub fn unpack_formatted_str(value: &FormattedInput<String>) -> Result<String, anyhow::Error> {
        match value {
            FormattedInput::SimpleString(s) => Ok(s.to_owned()),
            FormattedInput::BulkString(s) => Ok(s.to_owned()),
            _ => Err(anyhow::anyhow!("Expected command to be a bulk string"))
        }
    }
    
}