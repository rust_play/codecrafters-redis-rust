use std::{collections::HashMap, sync::Arc, time::Duration};
use tokio::{sync::Mutex, time::Instant};

#[derive(Debug)]
pub struct DBStorage {
    value: String, 
    time_out_value: Option<Duration>,
    time_out_timestamp: Instant
}

impl DBStorage {
    pub fn new(value: String, time_out_value: Option<u64>) -> Self {
      DBStorage{
        value,
        time_out_value: time_out_value.map(|time| Duration::from_millis(time)),
        time_out_timestamp: Instant::now()
      }
    }
    pub fn has_command_expired(&self) -> bool {
        match self.time_out_value {
            Some(expiry) => self.time_out_timestamp.elapsed() > expiry,
            None => false,
        }   
    }
}

#[derive(Debug, Clone)]
pub struct DBHandle( Arc<Mutex::< HashMap<String, DBStorage>> >);
impl DBHandle {

    pub fn new() -> Self {
       Self(Arc::new(Mutex::new(HashMap::new())))
    }
       
    pub async fn set<K>(&mut self, key: K, db_storage: DBStorage) where K: Into<String> {
        self.0.lock().await.insert(key.into(), db_storage);
    }

     pub async fn get<K>(&mut self, key: K) -> Option<String> where K: Into<String> {

        let mutex = self.0.lock().await;
        let hash_value = mutex.get(&key.into());
        let db_data = match hash_value {
            Some(str) => Some(str),
            None => None,
        };
       
        if let Some(_line) = db_data
        {
           if db_data.unwrap().to_owned().has_command_expired(){
            return None;
           } else {
            return Some(db_data.unwrap().value.to_string());
           }
        } else {
           return None;
        };
    }
}
