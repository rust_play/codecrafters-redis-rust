use std::{sync::Arc, time::Duration};
use redis_starter_rust::{command, db::DBHandle, resp};
use resp::ResponseHandler;
use tokio::{net::TcpListener, time::sleep};
use command::Command;

#[tokio::main]
async fn main() {
    // You can use print statements as follows for debugging, they'll be visible when running tests.
    println!("Logs from your program will appear here!");

    let listener = TcpListener::bind("127.0.0.1:6379").await.unwrap();
    let db_redis = DBHandle::new();
    let db_redis_arc = Arc::new(db_redis);
 
    loop {
        let db_redis_clone = Arc::clone(&db_redis_arc);
        let stream = listener.accept().await;
        match stream {
            Ok((_stream, _)) => {
                println!("accepted new connection");
                tokio::spawn(async move {
                     handle_connection(_stream, Arc::clone(&db_redis_clone)).await
                });  
            }
            Err(e) => {
                println!("error: {}", e);
            }
        }
    }
}

pub async fn redis_server_test(listener: tokio::net::TcpListener) {
    println!("Starting Redis server");
    let db_redis = DBHandle::new();
    let db_redis_arc = Arc::new(db_redis);
    loop {
        let db_redis_clone = Arc::clone(&db_redis_arc);
        match listener.accept().await {
            Ok((_stream, addr)) => {
                tokio::spawn(async move {
                    println!("Client: Accepted new connection: {:?}", addr);
                    handle_connection(_stream, db_redis_clone).await
                });
            }
            Err(e) => println!("Error: client not accepted: {}", e),
        }
    }
}

async fn handle_connection(stream: tokio::net::TcpStream, db_redis: Arc<DBHandle>  ) {
   let mut response_handler =  ResponseHandler::new(stream);
      println!("Starting read loop");
      loop {
          let db_redis_clone = Arc::clone(&db_redis);
          if let Some(value) = response_handler.read_stream().await.unwrap() {
            println!("Incoming value {:?}", value);
            let response = if let Some(v) = Some(value) {
                let (command, args) = Command::extract_command(v).unwrap();
                let resp = Command::execute_command(command.as_str(), args, db_redis_clone);
                resp.await
            } else {
                break;
            }; 
            response_handler.write_stream(response).await.unwrap();
            response_handler.buffer_flush().await;
          } else {
            println!("No message");
          }        
      }
}


#[cfg(test)]
//TEST STEP 5
 mod test_case_step_5 { 

    use std::{net::{IpAddr, Ipv4Addr, SocketAddr}};
    use tokio::{net::TcpListener, task};
    use redis_starter_rust::client::Client;
    
   #[tokio::test]
    async fn test_simple_string() {

        const SIMPLE_STRING: &[u8] = b"+OK\r\n";
        let listener = TcpListener::bind("127.0.0.1:6377").await.unwrap();
          //Start Redis server - test
          task::spawn(async move {
            super::redis_server_test(listener).await;
        });
     
        // Client-1
        let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 6377);
        let mut client_1 = Client::new(addr).await.expect("Connect Redis client");

        // Send request and get response
        let resp_1 = client_1
            .send_request(SIMPLE_STRING)
            .await
            .expect("Send echo request 900000");

        let expected_resp_1 = b"+OK\r\n";
        assert_eq!(&resp_1, expected_resp_1); 

        // Client-1
        let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 6377);
        let mut client_1 = Client::new(addr).await.expect("Connect Redis client");

        let resp_2 = client_1
            .send_request(SIMPLE_STRING)
            .await
            .expect("Send echo request 900000");

        let expected_resp_2 = b"+OK\r\n";
        assert_eq!(&resp_2, expected_resp_2); 
        
    }

 
    #[tokio::test]
    async fn test_bulk_string() {

        const BULK_STRING: &[u8] = b"$4\r\nBulk\r\n";    
        let listener = TcpListener::bind("127.0.0.1:6378").await.unwrap();
          //Start Redis server - test
          task::spawn(async move {
            super::redis_server_test(listener).await;
        });
     
        // Client-1
        let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 6378);
        let mut client_1 = Client::new(addr).await.expect("Connect Redis client");

       // Send request and get response
        let resp_1 = client_1
            .send_request(BULK_STRING)
            .await
            .expect("Send echo request 1");

        let expected_resp_1 = b"$4\r\nBulk\r\n";
        assert_eq!(&resp_1, expected_resp_1); 

    }

    #[tokio::test]
    async fn test_array_string() {

        const ARRAY_STRING: &[u8] = b"*2\r\n$4\r\necho\r\n$5\r\nARRAY\r\n";
        let listener = TcpListener::bind("127.0.0.1:6379").await.unwrap();
          //Start Redis server - test
          task::spawn(async move {
            super::redis_server_test(listener).await;
        });
     
        // Client-1
        let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 6379);
        let mut client_1 = Client::new(addr).await.expect("Connect Redis client");

        // Send request and get response
        let resp_1 = client_1
            .send_request(ARRAY_STRING)
            .await
            .expect("Send echo request 1");

        let expected_resp_1 = b"$5\r\nARRAY\r\n";
        assert_eq!(&resp_1, expected_resp_1); 

         // Client-2
        let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 6379);
        let mut client_2 = Client::new(addr).await.expect("Connect Redis client");
 
         //Send request and get response
        let resp_2 = client_2
             .send_request(ARRAY_STRING)
             .await
             .expect("Send echo request 1");
 
         let expected_resp_2 = b"$5\r\nARRAY\r\n";
         assert_eq!(&resp_2, expected_resp_2);

    }
}


//TEST STEP 6
mod test_case_step_6 {

    use std::{net::{IpAddr, Ipv4Addr, SocketAddr}};
    use tokio::{net::TcpListener, task};
    use redis_starter_rust::client::Client;
    
    #[cfg(test)]
    #[tokio::test]
    async fn test_set_string() {

        const FOO_BAR_STRING: &[u8] = b"*3\r\n$3\r\nset\r\n$5\r\ngrape\r\n$9\r\nraspberry\r\n";
        const FOO_STRING: &[u8] = b"*2\r\n$3\r\nget\r\n$5\r\ngrape\r\n";
        const BAD_STRING: &[u8] = b"*2\r\n$3\r\nget\r\n$3\r\nbad\r\n";

        let listener = TcpListener::bind("127.0.0.1:6371").await.unwrap();
          //Start Redis server - test
          task::spawn(async move {
            super::redis_server_test(listener).await;
        });
     
        // Client-1
        let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 6371);
        let mut client_1 = Client::new(addr).await.expect("Connect Redis client");

        // Send request and get response
        let resp_1 = client_1
            .send_request(FOO_BAR_STRING)
            .await
            .expect("Send echo request 1");

        let expected_resp_1 = b"+OK\r\n";
        assert_eq!(&resp_1, expected_resp_1); 

        // Client-1
        let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 6371);
        let mut client_1 = Client::new(addr).await.expect("Connect Redis client");

      // Send request and get response
        let resp_2 = client_1
            .send_request(FOO_STRING)
            .await
            .expect("Send echo request 1");

        let expected_resp_2 = b"$9\r\nraspberry\r\n";
        assert_eq!(resp_2,expected_resp_2); 

        // Client-1
        let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 6371);
        let mut client_1 = Client::new(addr).await.expect("Connect Redis client");

        // Send request and get response
        let resp_3 = client_1
        .send_request(BAD_STRING)
        .await
        .expect("Send echo request 1");

        let expected_resp_3 = b"$-1\r\n";
        assert_eq!(resp_3,expected_resp_3);

    }
}


//TEST STEP 7
mod test_case_step_7 {

    use std::{net::{IpAddr, Ipv4Addr, SocketAddr}};
    use tokio::{net::TcpListener, task};
    use redis_starter_rust::client::Client;
    
    #[cfg(test)]
    #[tokio::test]
    async fn test_set_exp_cmd() {
        use std::time::Duration;

        use tokio::time::sleep;

        const SET_EXP_CMD: &[u8] = b"*5\r\n$3\r\nset\r\n$5\r\napple\r\n$10\r\nstrawberry\r\n$2\r\npx\r\n$3\r\n100\r\n";
        const EXP_STRING: &[u8] = b"*2\r\n$3\r\nget\r\n$5\r\napple\r\n";

        let listener = TcpListener::bind("127.0.0.1:6360").await.unwrap();
          //Start Redis server - test
          task::spawn(async move {
            super::redis_server_test(listener).await;
        });
     
        // Client-1
        let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 6360);
        let mut client_1 = Client::new(addr).await.expect("Connect Redis client");

        // Send request and get response
        let resp_1 = client_1
            .send_request(SET_EXP_CMD)
            .await
            .expect("Send echo request 1");

        let expected_resp_1 = b"+OK\r\n";
        assert_eq!(&resp_1, expected_resp_1); 


         // Client-1
         let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 6360);
         let mut client_1 = Client::new(addr).await.expect("Connect Redis client");
 
         // Send request and get response
         let resp_1 = client_1
             .send_request(EXP_STRING)
             .await
             .expect("Send echo request 1");
 
         let expected_resp_1 = b"$10\r\nstrawberry\r\n";
         assert_eq!(&resp_1, expected_resp_1); 

          // Client-1
        let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 6360);
        let mut client_1 = Client::new(addr).await.expect("Connect Redis client");

        sleep(Duration::from_millis(101)).await;        
        // Send request and get response
        let resp_1 = client_1
            .send_request(EXP_STRING)
            .await
            .expect("Send echo request 1");

        let expected_resp_1 = b"$-1\r\n";
        assert_eq!(&resp_1, expected_resp_1); 
    }
}


