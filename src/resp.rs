use bytes::BytesMut;
use tokio::{net::TcpStream, io::{AsyncReadExt, AsyncWriteExt}};
use anyhow::Result;

pub trait RedisFormat<T> {
    fn serialize( self ) -> T;
}

#[derive(Clone, Debug)]
pub enum FormattedInput<T> {
  NoneString(T),  
  SimpleString(T),
  BulkString(T),   
  Array(Vec<FormattedInput<T>>)
}

impl RedisFormat<String> for FormattedInput<String> {
    fn serialize( self ) -> String {
        match self {
            FormattedInput::NoneString(s) => format!("${}\r\n", s),
            FormattedInput::SimpleString(s) => format!("+{}\r\n", s),
            FormattedInput::BulkString(s) => format!("${}\r\n{}\r\n", s.chars().count(), s),
            _ => panic!("Error: Value not supported. Serialize not allowed."),
        }
    }
}

pub struct ResponseHandler {
    stream: TcpStream,
    buffer: BytesMut,
}

impl ResponseHandler {

    pub fn new(stream: TcpStream) -> Self {
        ResponseHandler {
            stream, 
            buffer: BytesMut::with_capacity(64)
        }
    }

    pub async fn read_stream(&mut self) -> Result<Option<FormattedInput<String>>> {
        let bytes_read = self.stream.read_buf(&mut self.buffer).await.unwrap();
        let formatted_stream = Self::process_stream(&self.buffer).unwrap();
        if bytes_read == 0 {
            return Ok(None);
        } else {
            return Ok(Some(formatted_stream.0));
        }
    }

    pub async fn buffer_flush(&mut self) {
        self.buffer = BytesMut::with_capacity(64);
        self.stream.flush().await.unwrap();
    }

    pub fn process_stream(buffer: &BytesMut) -> Result<(FormattedInput<String>, usize)> {
        if !buffer.is_empty() {
            match buffer[0] as char {
                '+' => Self::parse_simple_string(&buffer),
                '*' => Self::parse_array(&buffer),
                '$' => Self::parse_bulk_string(&buffer),
                _ => Err(anyhow::anyhow!("Error: Unknown type {:?}", buffer)),
            }
        } else {
            Ok((FormattedInput::NoneString("-1".to_string()), 0))
        }
    }

    pub async fn write_stream(&mut self, formatted_input: FormattedInput<String>) -> Result<usize, ()> {
        Ok(self.stream.write(formatted_input.serialize().as_bytes()).await.unwrap())
    }

    fn parse_simple_string(buffer: &BytesMut) -> Result<(FormattedInput<String>, usize)> {
        if let Some((sub_string, len)) = Self::read_until_lf(&buffer[..]) {
            let sub_string = String::from_utf8(sub_string.to_vec()).unwrap();
            return Ok((FormattedInput::SimpleString(sub_string), len))
        }
        return Err(anyhow::anyhow!("Invalid string {:?}", buffer));
    }

    fn parse_bulk_string(buffer: &BytesMut) -> Result<(FormattedInput<String>, usize)> {
        let (bulk_len, bytes_consumed) = if let Some((sub_buffer, sub_buffer_len)) = Self::read_until_lf(&buffer[..]) {
            let bulk_str_len = Self::parse_int(sub_buffer)?;
            (bulk_str_len, sub_buffer_len)
        } else {
            return Err(anyhow::anyhow!("Invalid array format {:?}", buffer));
        };

        let end_of_bulk = bytes_consumed + bulk_len as usize;
        let total_parsed = end_of_bulk + 2;
        Ok((FormattedInput::BulkString(String::from_utf8(buffer[bytes_consumed..end_of_bulk].to_vec())?), total_parsed))
    }

    fn parse_array(buffer: &BytesMut) -> Result<(FormattedInput<String>, usize)> {

        let (arr_length, mut bytes_consumed) = if let Some((line, len)) = Self::read_until_lf(&buffer[..])
        {
            let arr_length = Self::parse_int(line)?;
            (arr_length, len)
        } else {
            return Err(anyhow::anyhow!("Not a valid array format {:?}", buffer));
        };
        let mut items: Vec<FormattedInput<String>> = vec![];
        for _ in 0..arr_length {
            let (array_item, len) = Self::process_stream(&BytesMut::from(&buffer[bytes_consumed..]))?;
            items.push(array_item);
            bytes_consumed += len;
        }
        return Ok((FormattedInput::Array(items), bytes_consumed));
    }

    fn read_until_lf(mut buffer: &[u8]) -> Option<(&[u8], usize)> {
        buffer = &buffer[1..];
        let pos = buffer.iter().position(|&x| x == b'\n');
        for i in 1..buffer.len() {
            if i == pos.unwrap() {
                return Some((&buffer[0..(i-1)], i + 2));
            }
        }        
        return None;
    }

    fn parse_int(buffer: &[u8]) -> Result<i64, anyhow::Error> {
        Ok(String::from_utf8(buffer.to_vec())?.parse::<i64>()?)
    }

}